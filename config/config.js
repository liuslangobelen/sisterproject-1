// Initialize Firebase
const admin = require("firebase");

//let serviceAccount = require("path/to/tibiko-backend-firebase-adminsdk-8p2h8-e6fa855538.json");
let config = {
    apiKey: "AIzaSyB3ZXjYTXskhXbYqPr4tQvAejTgLMUclNw",
    authDomain: "sisterproject-1.firebaseapp.com",
    databaseURL: "https://sisterproject-1.firebaseio.com",
    projectId: "sisterproject-1",
    storageBucket: "sisterproject-1.appspot.com",
    messagingSenderId: "891788486772",
    appId: "1:891788486772:web:8d1f3d8711cc2d4ed60921"
  };

//config.credential = admin.credential.cert(serviceAccount);
//{
//  credential: admin.credential.cert(serviceAccount),
//  databaseURL: "https://tibiko-backend.firebaseio.com"
//}
admin.initializeApp(config);
const database = admin.database();

module.exports = database;
